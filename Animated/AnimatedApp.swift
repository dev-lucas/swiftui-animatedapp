//
//  AnimatedApp.swift
//  Animated
//
//  Created by Lucas Gomes on 07/05/23.
//

import SwiftUI

@main
struct AnimatedApp: App {
    var body: some Scene {
        WindowGroup {
//            OnboardingView()
            ContentView()
        }
    }
}
